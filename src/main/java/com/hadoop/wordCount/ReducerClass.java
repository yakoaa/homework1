package com.hadoop.wordCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;
import java.util.Iterator;

public class ReducerClass extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable>  {

    public void reduce(Text key, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
        int sum = 0;
        //для кадлого из значений на выходе из маппера производим подсчет количество
        //из вида {Chrome [1,1,1,1,1,1,1,1,1]} превращаем в [Chrome, 9] и тд
        while(values.hasNext()){
            sum = sum + values.next().get();
        }

        output.collect(key, new IntWritable(sum));
    }
}
