package com.hadoop.wordCount;

import eu.bitwalker.useragentutils.UserAgent;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

import java.io.IOException;

public class MapperClass extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {
    /*a(3), b(1), c(4), d(5)*/
    /*a(3)  -   For input information from a)-2 count how many users of IE, Mozilla or other were detected*/
    /*b(1)  -   Output: CSV file*/
    /*c(4)  -   Additional Requirements: Several input files were used for testing*/
    /*d(5)  -   Report includes (mandatory): Quick build and deploy manual (commands, OS requirements etc)*/
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> output, Reporter reporter) throws IOException {
        //логи http сервера похож на следующую строку:
        //ipAddr                                            request                                 HTTPCode    bytes    user-info                  user-agent
        //99.168.127.53 - - [20/May/2010:07:34:13 +0100]    "GET /media/img/m-inact.gif HTTP/1.1"   200         2571    "http://www.example.com/"   "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_1_3 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7E18 Safari/528.16"
        //между аттрибутами стоит знак табуляции, соответвенно, получая строку, необходимо разбивать её
        //и получать 6 аттрибут - user-agent в названии которого зашифрован браузер
        //конвертировать user-agent мы будет при помощи сторонней библитеки, на вход подаем user-agent, на выходе получим название браузера

        //ПОЛУЧАЕМ ЛИНИЮ НА ВХОД
        String line = value.toString();

        //СПЛИТИМ СТРОКУ НА "СЛОВА" ПО ТАБАМ
        String[] words = line.split("\t");
        //КОНВЕРТИРУЕМ 5 "СЛОВО" В НАЗВАНИЕ БРАУЗЕРА ПРИ ПОМОЩИ ВСПОМОГАТЕЛЬНОГО МОДУЛЯ И ПОЛУЧАЕМ НАЗВАНИЕ БРАУЗЕРА
        UserAgent userAgent = UserAgent.parseUserAgentString(words[5]);
        String browserName = userAgent.getBrowser().getName();

        //ПОЛУЧЕНОЕ ИМЯ БРАУЗЕРА СОБИРАЕМ ДЛЯ ДАЛЬНЕШЕГО ПОДСЧЕТА
        word.set(browserName);
        output.collect(word, one);
    }
}