package com.hadoop;

import com.hadoop.wordCount.MapperClass;
import com.hadoop.wordCount.ReducerClass;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {

        System.out.println(String.format("Hello big hadoop World!"));

        JobConf conf = new JobConf(Application.class);

        conf.setJobName("Homework1_Hadoop");
        //выставляем форматы значений для key и value
        conf.setOutputKeyClass(Text.class);
        conf.setOutputValueClass(IntWritable.class);

        //определяем мапперы и редьюсеры для джоба
        conf.setMapperClass(MapperClass.class);
        conf.setCombinerClass(ReducerClass.class);
        conf.setReducerClass(ReducerClass.class);

        //выставляем формат входных и выходных файлов
        conf.setInputFormat(TextInputFormat.class);
        conf.setOutputFormat(TextOutputFormat.class);
        //выставляем сепаратором запятую, для записи в формате csv
        conf.set("mapreduce.output.textoutputformat.separator", ",");
        //выставляем количество редьюсеров
        conf.setNumReduceTasks(1);

        //получаем входную и выходную дирректорию для загрузки/сохранения файлов
        FileInputFormat.setInputPaths(conf, new Path(args[0]));
        FileOutputFormat.setOutputPath(conf, new Path(args[1]));
        //запускаем джоб на обработку
        JobClient.runJob(conf);
    }
}
