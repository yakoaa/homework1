package com.hadoop.wordCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReducerClassTest {

    ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;

    @Before
    public void setUp() {
        ReducerClass reducer = new ReducerClass();
        ReduceDriver.newReduceDriver(reducer);
    }

    @Test
    public void reduceChromeTwo() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();

        values.add(new IntWritable(1));
        values.add(new IntWritable(1));

        reduceDriver.withInput(new Text("Chrome"), values);
        reduceDriver.withOutput(new Text("Chrome"), new IntWritable(2));
        reduceDriver.runTest();
    }

}
