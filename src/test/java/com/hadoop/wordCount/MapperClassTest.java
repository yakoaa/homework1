package com.hadoop.wordCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.MapDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class MapperClassTest {

    private MapDriver <LongWritable, Text, Text, IntWritable> mapDriver;

    @Before
    public void setUp() {
        MapperClass mapper = new MapperClass();
        mapDriver = MapDriver.newMapDriver(mapper);
    }

    @Test
    public void mapEdge() throws IOException {
        String logLine = "127.0.0.1 - - [01/11/2019:01:32:23 +0300]\tGET /heathcheck HTTP/1.0\t200\t400\t-\tMozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586";
        mapDriver.withInput(new LongWritable(), new Text(logLine));
        mapDriver.withOutput(new Text("Microsoft Edge (layout engine 13)"), new IntWritable(1));
        mapDriver.runTest();
    }

    @Test
    public void mapSafari() throws IOException {
        String logLine = "127.0.0.1 - - [01/11/2019:01:32:23 +0300]\tGET /heathcheck HTTP/1.0\t200\t400\t-\tMozilla/5.0 (Windows; U; ; en-EN) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.8.0";
        mapDriver.withInput(new LongWritable(), new Text(logLine));
        mapDriver.withOutput(new Text("Safari"), new IntWritable(1));
        mapDriver.runTest();
    }

    @Test
    public void mapSpider() throws IOException {
        String logLine = "67.195.114.50 - - [20/May/2010:07:35:27 +0100]\tGET /post/261556/ HTTP/1.0\t404\t15\t-\tMozilla/5.0 (compatible; Yahoo! Slurp/3.0; http://help.yahoo.com/help/us/ysearch/slurp)";
        mapDriver.withInput(new LongWritable(), new Text(logLine));
        mapDriver.withOutput(new Text("Robot/Spider"), new IntWritable(1));
        mapDriver.runTest();
    }

    @Test
    public void mapChrome() throws IOException {
        String logLine = "127.0.0.1 - - [01/11/2019:01:32:23 +0300]\tGET /heathcheck HTTP/1.0\t200\t400\t-\tMozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.28.3 (KHTML, like Gecko) Version/3.2.3 ChromePlus/4.0.222.3 Chrome/4.0.222.3 Safari/525.28.3";
        mapDriver.withInput(new LongWritable(), new Text(logLine));
        mapDriver.withOutput(new Text("Chrome"), new IntWritable(1));
        mapDriver.runTest();
    }

}